'use strict';

/* Controllers */

app
// sigin controller
    .controller('SignupFormController', function ($scope, $http , $location ,$state) {
        $scope.name = "";
        $scope.email = "";
        $scope.password = "";
        $scope.sendPost = function() {
            var data = {
                'name' : $scope.name,
                'email': $scope.email,
                'password': $scope.password
            };
            // console.log(data);
            $http.post("http://localhost:1337/user/create", data).success(function(data, status) {
                // $scope.token = data.token;
                // sessionStorage.AuthToken = $scope.token;
                // console.log(sessionStorage.AuthToken);
                $state.go('access.signin');
            }).error(function(status)
            {
                $scope.error = 'Please Enter Valid Info!';
            });
        };

    });
'use strict';

/* Controllers */

app
// sigin controller
    .controller('SigninFormController', function ($scope, $http , $location ,$state) {

        sessionStorage.AuthToken = "";

        $scope.email = "";
        $scope.password = "";
        $scope.sendPost = function() {
            var data = {
                'email': $scope.email,
                'password': $scope.password
            };
            console.log(data);
            // return 1;
            $http.post("http://localhost:1337/session", data).success(function(data, status) {
                $scope.token = data.token;
                sessionStorage.AuthToken = $scope.token;
                //console.log(sessionStorage.AuthToken);
                $state.go('app.dashboard-v1');
            }).error(function(status)
            {
                
                $scope.error = status.message;
            });
        };

    });
